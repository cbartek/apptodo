CREATE TABLE todos (
    id int unsigned primary key auto_increment,
    text varchar(100) not null,
    done bit
);

INSERT INTO todos (text, done) values ('Done todo', 1);
INSERT INTO todos (text, done) values ('Undone todo', 0);

