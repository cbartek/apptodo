package pl.cholewa.appToDo.hello;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServlet;


@RestController
class HelloController extends HttpServlet {
    private final Logger logger= LoggerFactory.getLogger(HelloController.class);

    private HelloService service;

    HelloController(HelloService service){
        this.service=service;
    }

    @GetMapping("/api")
    String welcome(){
        return welcome(null, null);
    }
    @GetMapping(value = "/api", params= {"lang"})
    String welcome(@RequestParam Integer lang){
        return welcome(lang, null);
    }
    @GetMapping(value = "/api", params={"name"})
    String welcome(@RequestParam String name){
        return welcome(null, name);
    }
    @GetMapping(value = "/api", params = {"lang", "name"})
    String welcome(@RequestParam("lang") Integer langId, @RequestParam String name ){
        logger.info("Request got");
        return service.prepareGreeting(name, langId);
    }
}
